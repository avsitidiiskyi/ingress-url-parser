from ast import literal_eval as dictionary
from aiohttp import ClientSession
from datetime import datetime
import ruamel.yaml as dumper
import asyncio
import json
slash = '/'


def get_initial_url(input_file):
    out_json_file = 'reformatted.json'
    with open(input_file, encoding="utf8") as yaml_file, open(out_json_file, 'w') as json_file:
        json.dump(dumper.YAML(typ='safe').load(yaml_file), json_file, indent=2)

        def json_reader():
            with open("reformatted.json", "r") as readable:
                return readable.read()
    return str(f"https://{dictionary(json_reader())['spec']['rules'][0]['host']}{slash}")


def get_string(input_file):
    out_json_file = 'reformatted.json'
    with open(input_file, encoding="utf8") as yaml_file, open(out_json_file, 'w') as json_file:
        json.dump(dumper.YAML(typ='safe').load(yaml_file), json_file, indent=2)

        def json_reader():
            with open("reformatted.json", "r") as readable:
                return readable.read()
    return dictionary(json_reader())['spec']['rules'][0]['http']['paths']


def reformat_file():
    string = get_string('ingress.yaml')
    return str([x['path'][2:-6] for x in string[1:-1] if x['path'][-1] == "$"][0:])


def array_of_urls(init_url):
    text_splitter_in_array = (reformat_file()[1:-1].replace(",", "|").replace(" ", "").replace("'", "").split("|"))
    return [init_url + x+slash for x in text_splitter_in_array]


async def check_if_urls_are_valid(url, session):
    async with session.head(url) as response:
        status_code = int([response.history[0].status if response.history else response.status][0])
        try:
            assert status_code < 399
        except AssertionError:
            print(f"{url} -- {status_code} status code, {response.headers['Server']}")
        return await response.read()


async def bound_check_if_urls_are_valid(semaphore, url, session):
    async with semaphore:
        await check_if_urls_are_valid(url, session)


async def run(urls):
    async with ClientSession() as session:
        for url in urls:
            tasks = [asyncio.ensure_future(bound_check_if_urls_are_valid(asyncio.Semaphore(1000), url, session))]
        await asyncio.gather(*tasks)


if __name__ == '__main__':
    start = datetime.now()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.ensure_future(run(array_of_urls(get_initial_url('ingress.yaml')))))

    print(f"Elapsed time: {datetime.now() - start}")
